<?php

namespace MSU\OAuth2;

class Client {
  const MSU_AUTH_URL = 'https://oauth.dev.ais.msu.edu/oauth/authorize';
  const MSU_TOKEN_URL = 'https://oauth.dev.ais.msu.edu/oauth/token';

  protected $php_oauth2    = null;
  protected $access_token  = null;
  protected $settings      = null;
  protected $client_id     = null;
  protected $client_secret = null;
  protected $redirect_url  = null;

  public function __construct($client_id, $client_secret, $redirect_url) {
    $this->client_id     = $client_id;
    $this->client_secret = $client_secret;
    $this->redirect_url  = $redirect_url;
    $this->php_oauth2    = new \OAuth2\Client($this->client_id,
      $this->client_secret);
  }

  public function authenticate() {
    header('Location: ' . $this->getAuthUrl());
    die('Redirect');
  }

  public function setAccessToken($token) {
    $this->php_oauth2->setAccessToken($token);
    $this->php_oauth2->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_URI);
    $this->access_token = $token;
  }

  public function getAccessToken($code = null, array $params = array()) {
    if($code !== null) {
      $this->retrieveToken($code, $params);
    }

    return $this->access_token;
  }

  public function fetch($url = 'https://oauth.dev.ais.msu.edu/oauth/me') {
    if($this->access_token !== null) {
      $response = $this->php_oauth2->fetch($url);
      return $response['result'];
    } else {
      return '';
    }
  }

  private function retrieveToken($code = null, array $params = array()) {
    $params['code'] = $code;
    $params['redirect_uri'] = $this->redirect_url;

    $response = $this->php_oauth2->getAccessToken(self::MSU_TOKEN_URL,
      'authorization_code', $params);
    $this->setAccessToken($response['result']['access_token']);
  }

  private function getAuthUrl() {
    return $this->php_oauth2->getAuthenticationUrl(self::MSU_AUTH_URL,
      $this->redirect_url);
  }
}
